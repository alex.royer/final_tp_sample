# Dockerfile

FROM tomcat:8.5.41-jre8-alpine

MAINTAINER "Alex"

COPY webapp/target/webapp.war /usr/local/tomcat/webapps
